#include <dht11.h>
#define DHT11PIN 7

dht11 DHT11;

void setup()
{
  Serial.begin(9600);
}

void loop()
{

  int chk = DHT11.read(DHT11PIN);
  Serial.println(DHT11.humidity);
  Serial.println(DHT11.temperature);
  delay(1000);
}
